<?php

use App\Category;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:web')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/categories/','SubmissionController@getCategories');
Route::get('/submissions/{id}', 'SubmissionController@show');
Route::get('/submissions/', 'SubmissionController@all');
Route::get('/submissions/category/{slug}', 'SubmissionController@getUserSubmissionsByCategory');
