/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 59);
/******/ })
/************************************************************************/
/******/ ({

/***/ 59:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(60);


/***/ }),

/***/ 60:
/***/ (function(module, exports, __webpack_require__) {

window.Zoomerang = __webpack_require__(61);
window.ajaxchimp = __webpack_require__(62);
$(function () {
  navbar.init();

  pricing_charts.init();

  global_notifications.init();

  ecommerce.init();

  retina.init();

  zoomerang.init();

  animation.init();

  off_canvas.init();

  newsletter.init();
});

var animation = {
  lastScrollY: 0,
  ticking: false,
  _this: null,
  elements: null,

  init: function init() {
    _this = this;
    _this.elements = $('[data-animate]');

    window.addEventListener('scroll', _this.onScroll, false);
    _this.update();
  },

  onScroll: function onScroll() {
    _this.lastScrollY = window.scrollY;
    _this.requestTick();
  },

  requestTick: function requestTick() {
    if (!_this.ticking) {
      requestAnimationFrame(_this.update);
      _this.ticking = true;
    }
  },

  update: function update() {
    for (var i = _this.elements.length - 1; i >= 0; i--) {
      var $el = $(_this.elements[i]);

      if ($el.hasClass($el.data("animate"))) {
        continue;
      }

      if (_this.isInViewport($el)) {
        _this.triggerAnimate($el);
      }
    }

    // allow further rAFs to be called
    _this.ticking = false;
  },

  isInViewport: function isInViewport($element) {
    var top_of_element = $element.offset().top;
    var bottom_of_element = $element.offset().top + $element.outerHeight();
    var bottom_of_screen = $(window).scrollTop() + $(window).height();
    var top_of_screen = $(window).scrollTop();

    return bottom_of_screen > top_of_element && top_of_screen < bottom_of_element;
  },

  triggerAnimate: function triggerAnimate($element) {
    var effect = $element.data("animate");
    var infinite = $element.data("animate-infinite") || null;
    var delay = $element.data("animate-delay") || null;
    var duration = $element.data("animate-duration") || null;

    if (infinite !== null) {
      $element.addClass("infinite");
    }

    if (delay !== null) {
      $element.css({
        "-webkit-animation-delay": delay + "s",
        "-moz-animation-delay": delay + "s",
        "animation-delay": delay + "s"
      });
    }

    if (duration !== null) {
      $element.css({
        "-webkit-animation-duration": duration + "s",
        "-moz-animation-duration": duration + "s",
        "animation-duration": duration + "s"
      });
    }

    $element.addClass("animated " + effect).one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend", function () {
      $element.addClass("animated-end");
    });
  }
};

var navbar = {
  init: function init() {
    if (!window.utils.isMobile()) {
      this.dropdownHover();
      this.transparentFixed();
    }

    // prevent dropdown link click to hide dropdown
    $('.navbar-nav .dropdown-item').click(function (e) {
      e.stopPropagation();
    });

    // toggle for dropdown submenus
    $('.dropdown-submenu .dropdown-toggle').click(function (e) {
      e.preventDefault();
      $(this).parent().toggleClass('show');
      $(this).siblings('.dropdown-menu').toggleClass('show');
    });

    this.fixedBottom();

    // offcanvas collapsable
    $('[data-toggle="offcanvas"]').on('click', function () {
      $('.offcanvas-collapse').toggleClass('open');
    });
  },

  dropdownHover: function dropdownHover() {
    var $dropdowns = $('.navbar-nav .dropdown');
    $dropdowns.each(function (index, item) {
      var $item = $(this);

      $item.hover(function () {
        $item.addClass('show');
      }, function () {
        $item.removeClass('show');
      });
    });
  },

  transparentFixed: function transparentFixed() {
    var $navbar = $('.navbar');

    if ($navbar.hasClass('bg-transparent') && $navbar.hasClass('fixed-top')) {
      var navbarTop = $navbar.offset().top + 1;

      var scrollingFn = function scrollingFn() {
        var offsetTop = window.scrollY || window.pageYOffset;

        if (offsetTop >= navbarTop && $navbar.hasClass('bg-transparent')) {
          $navbar.removeClass('bg-transparent');
        } else if (offsetTop < navbarTop && !$navbar.hasClass('bg-transparent')) {
          $navbar.addClass('bg-transparent');
        }
      };

      $(window).scroll(scrollingFn);
    }
  },

  fixedBottom: function fixedBottom() {
    $navbar = $('.navbar');

    if ($navbar.hasClass('navbar-fixed-bottom')) {
      var navbarTop = $navbar.offset().top + 1;

      var scrollingFn = function scrollingFn() {
        var offsetTop = window.scrollY || window.pageYOffset;

        if (offsetTop >= navbarTop && !$navbar.hasClass('navbar-fixed-bottom--stick')) {
          $navbar.addClass('navbar-fixed-bottom--stick');
        } else if (offsetTop < navbarTop && $navbar.hasClass('navbar-fixed-bottom--stick')) {
          $navbar.removeClass('navbar-fixed-bottom--stick');
        }
      };
    }

    $(window).scroll(scrollingFn);
  }
};

var zoomerang = {
  init: function init() {
    Zoomerang.config({
      maxHeight: 730,
      maxWidth: 900
    }).listen('[data-trigger="zoomerang"]');
  }
};

var ecommerce = {
  init: function init() {
    this.displayCart();
    this.search();
  },

  displayCart: function displayCart() {
    var $cart = $(".store-navbar .cart"),
        $modal = $("#cart-modal"),
        timeout;

    var showModal = function showModal() {
      $modal.addClass("visible");

      clearTimeout(timeout);
      timeout = null;
    };

    var hideModal = function hideModal() {
      timeout = setTimeout(function () {
        $modal.removeClass("visible");
      }, 400);
    };

    $cart.hover(showModal, hideModal);
    $modal.hover(showModal, hideModal);
  },

  search: function search() {
    var $searchField = $('.store-navbar .search-field');
    var $searchInput = $searchField.find('.input-search');

    $searchInput.focus(function () {
      $searchField.addClass('focus');
    });

    $searchInput.blur(function () {
      $searchField.removeClass('focus');
    });
  }
};

var global_notifications = {
  init: function init() {
    setTimeout(function () {
      $(".global-notification").removeClass("uber-notification").addClass("uber-notification-remove");
    }, 5000);
  }
};

var pricing_charts = {
  init: function init() {
    var tabs = $(".pricing-charts-tabs .tab"),
        prices = $(".pricing-charts .chart header .price");

    tabs.click(function () {
      tabs.removeClass("active");
      $(this).addClass("active");

      var period = $(this).data("tab");
      var price_out = prices.filter(":not(." + period + ")");
      price_out.addClass("go-out");
      prices.filter("." + period + "").addClass("active");

      setTimeout(function () {
        price_out.removeClass("go-out").removeClass("active");
      }, 250);
    });
  }
};

var off_canvas = {
  init: function init() {
    var $offWrapper = $('.off-wrapper');
    var $toggler = $offWrapper.find('.navbar-toggler');
    var $offContent = $offWrapper.find('.off-wrapper-content');
    var $offMenu = $offWrapper.find('.off-wrapper-menu');

    $offContent.click(function () {
      $offWrapper.removeClass('active');
    });

    $toggler.click(function (e) {
      e.stopPropagation();
      $offWrapper.toggleClass('active');
    });

    $offMenu.find('.dropdown-item').click(function (e) {
      e.stopPropagation();
    });
  }
};

var newsletter = {
  init: function init() {
    var $form = $('.newsletter-form');
    var $feedback = $form.find('.newsletter-feedback');

    $form.ajaxChimp({
      callback: newsletterCallback,
      // UPDATE THIS LINK TO USE YOUR OWN LIST
      url: "http://wrapbootstrap.us16.list-manage.com/subscribe/post?u=0445d4d17445ee39d817296af&amp;id=1cfa1d2037"
    });

    function newsletterCallback(resp) {
      $feedback.removeClass('text-success text-danger text-white');

      if (resp.result === 'success') {
        $feedback.addClass('text-success');
      } else {
        $feedback.addClass('text-danger');
      }
    }
  }
};

var retina = {
  init: function init() {
    if (window.devicePixelRatio >= 1.2) {
      $("[data-2x]").each(function () {
        if (this.tagName == "IMG") {
          $(this).attr("src", $(this).attr("data-2x"));
        } else {
          $(this).css({ "background-image": "url(" + $(this).attr("data-2x") + ")" });
        }
      });
    }
  }
};

window.utils = {
  isFirefox: function isFirefox() {
    return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  },

  isSafari: function isSafari() {
    return navigator.userAgent.toLowerCase().indexOf('safari') > -1;
  },

  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // N milliseconds. If `immediate` is passed, trigger the function on the
  // leading edge, instead of the trailing.
  debounce: function debounce(func, wait, immediate) {
    var timeout;
    return function () {
      var context = this,
          args = arguments;
      var later = function later() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  },

  isMobile: function isMobile() {
    if (window.innerWidth <= 1024) {
      return true;
    } else {
      return false;
    }
  },

  parallax_text: function parallax_text($selector, extra_top) {
    extra_top = typeof extra_top !== 'undefined' ? extra_top : 0;
    var lastScrollY = 0;
    var ticking = false;

    window.addEventListener('scroll', onScroll, false);

    function onScroll() {
      lastScrollY = window.scrollY;
      requestTick();
    }

    function requestTick() {
      if (!ticking) {
        requestAnimationFrame(update);
        ticking = true;
      }
    }

    function update() {
      var scroll = lastScrollY,
          slowScroll = scroll / 1.4,
          slowBg = extra_top + slowScroll + "px",
          opacity,
          transform = "transform" in document.body.style ? "transform" : "-webkit-transform";

      if (scroll > 0) {
        opacity = (1000 - scroll * 2.7) / 1000;
      } else {
        opacity = 1;
      }

      $selector.css({
        "position": "relative",
        "top": slowBg,
        "opacity": opacity
      });

      ticking = false;
    }
  }
};

/***/ }),

/***/ 61:
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*
 * zoomerang.js - http://yyx990803.github.io/zoomerang/
 */

(function () {

    // webkit prefix helper
    var prefix = 'WebkitAppearance' in document.documentElement.style ? '-webkit-' : '';

    // regex
    var percentageRE = /^([\d\.]+)%$/;

    // elements
    var overlay = document.createElement('div'),
        wrapper = document.createElement('div'),
        target,
        parent,
        placeholder;

    // state
    var shown = false,
        lock = false,
        originalStyles;

    // options
    var options = {
        transitionDuration: '.4s',
        transitionTimingFunction: 'cubic-bezier(.4,0,0,1)',
        bgColor: '#fff',
        bgOpacity: 1,
        maxWidth: 300,
        maxHeight: 300,
        onOpen: null,
        onClose: null,
        onBeforeClose: null,
        onBeforeOpen: null

        // compatibility stuff
    };var trans = sniffTransition(),
        transitionProp = trans.transition,
        transformProp = trans.transform,
        transformCssProp = transformProp.replace(/(.*)Transform/, '-$1-transform'),
        transEndEvent = trans.transEnd;

    setStyle(overlay, {
        position: 'fixed',
        display: 'none',
        zIndex: 99998,
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        opacity: 0,
        backgroundColor: options.bgColor,
        cursor: prefix + 'zoom-out',
        transition: 'opacity ' + options.transitionDuration + ' ' + options.transitionTimingFunction
    });

    setStyle(wrapper, {
        position: 'fixed',
        zIndex: 99999,
        top: '50%',
        left: '50%',
        width: 0,
        height: 0
    });

    // helpers ----------------------------------------------------------------

    function setStyle(el, styles, remember) {
        checkTrans(styles);
        var s = el.style,
            original = {};
        for (var key in styles) {
            if (remember) {
                original[key] = s[key] || '';
            }
            s[key] = styles[key];
        }
        return original;
    }

    function sniffTransition() {
        var ret = {},
            trans = ['webkitTransition', 'transition', 'mozTransition'],
            tform = ['webkitTransform', 'transform', 'mozTransform'],
            end = {
            'transition': 'transitionend',
            'mozTransition': 'transitionend',
            'webkitTransition': 'webkitTransitionEnd'
        };
        trans.some(function (prop) {
            if (overlay.style[prop] !== undefined) {
                ret.transition = prop;
                ret.transEnd = end[prop];
                return true;
            }
        });
        tform.some(function (prop) {
            if (overlay.style[prop] !== undefined) {
                ret.transform = prop;
                return true;
            }
        });
        return ret;
    }

    function checkTrans(styles) {
        var value;
        if (styles.transition) {
            value = styles.transition;
            delete styles.transition;
            styles[transitionProp] = value;
        }
        if (styles.transform) {
            value = styles.transform;
            delete styles.transform;
            styles[transformProp] = value;
        }
    }

    var stylesToCopy = ['position', 'display', 'float', 'top', 'left', 'right', 'bottom', 'marginBottom', 'marginLeft', 'marginRight', 'marginTop', 'font', 'lineHeight', 'verticalAlign'];

    function copy(el, box) {
        var styles = getComputedStyle(el),
            ph = document.createElement('div'),
            i = stylesToCopy.length,
            key;
        while (i--) {
            key = stylesToCopy[i];
            ph.style[key] = styles[key];
        }
        setStyle(ph, {
            visibility: 'hidden',
            width: box.width + 'px',
            height: box.height + 'px',
            display: styles.display === 'inline' ? 'inline-block' : styles.display
        });
        if (options.deepCopy) {
            ph.innerHTML = el.innerHTML;
        } else {
            ph.textContent = el.textContent;
        }
        return ph;
    }

    var api = {

        config: function config(opts) {

            if (!opts) return options;
            for (var key in opts) {
                options[key] = opts[key];
            }
            setStyle(overlay, {
                backgroundColor: options.bgColor,
                transition: 'opacity ' + options.transitionDuration + ' ' + options.transitionTimingFunction
            });
            return this;
        },

        open: function open(el, cb) {

            if (shown || lock) return;

            target = typeof el === 'string' ? document.querySelector(el) : el;

            // onBeforeOpen event
            if (options.onBeforeOpen) options.onBeforeOpen(target);

            shown = true;
            lock = true;
            parent = target.parentNode;

            var p = target.getBoundingClientRect(),
                scale = Math.min(options.maxWidth / p.width, options.maxHeight / p.height),
                dx = p.left - (window.innerWidth - p.width) / 2,
                dy = p.top - (window.innerHeight - p.height) / 2;

            placeholder = copy(target, p);

            originalStyles = setStyle(target, {
                position: 'absolute',
                top: 0,
                left: 0,
                right: '',
                bottom: '',
                whiteSpace: 'nowrap',
                marginTop: -p.height / 2 + 'px',
                marginLeft: -p.width / 2 + 'px',
                cursor: prefix + 'zoom-out',
                transform: 'translate(' + dx + 'px, ' + dy + 'px)',
                transition: ''
            }, true);

            // deal with % width and height
            var wPctMatch = target.style.width.match(percentageRE),
                hPctMatch = target.style.height.match(percentageRE);
            if (wPctMatch || hPctMatch) {
                var wPct = wPctMatch ? +wPctMatch[1] / 100 : 1,
                    hPct = hPctMatch ? +hPctMatch[1] / 100 : 1;
                setStyle(wrapper, {
                    width: ~~(p.width / wPct) + 'px',
                    height: ~~(p.height / hPct) + 'px'
                });
            }

            // insert overlay & placeholder
            parent.appendChild(overlay);
            parent.appendChild(wrapper);
            parent.insertBefore(placeholder, target);
            wrapper.appendChild(target);
            overlay.style.display = 'block';

            // force layout
            var force = target.offsetHeight;

            // trigger transition
            overlay.style.opacity = options.bgOpacity;
            setStyle(target, {
                transition: transformCssProp + ' ' + options.transitionDuration + ' ' + options.transitionTimingFunction,
                transform: 'scale(' + scale + ')'
            });

            target.addEventListener(transEndEvent, function onEnd() {
                target.removeEventListener(transEndEvent, onEnd);
                lock = false;
                cb = cb || options.onOpen;
                if (cb) cb(target);
            });

            return this;
        },

        close: function close(cb) {

            if (!shown || lock) return;
            lock = true;

            // onBeforeClose event
            if (options.onBeforeClose) options.onBeforeClose(target);

            var p = placeholder.getBoundingClientRect(),
                dx = p.left - (window.innerWidth - p.width) / 2,
                dy = p.top - (window.innerHeight - p.height) / 2;

            overlay.style.opacity = 0;
            setStyle(target, {
                transform: 'translate(' + dx + 'px, ' + dy + 'px)'
            });

            target.addEventListener(transEndEvent, function onEnd() {
                target.removeEventListener(transEndEvent, onEnd);
                setStyle(target, originalStyles);
                parent.insertBefore(target, placeholder);
                parent.removeChild(placeholder);
                parent.removeChild(overlay);
                parent.removeChild(wrapper);
                overlay.style.display = 'none';
                placeholder = null;
                shown = false;
                lock = false;
                cb = typeof cb === 'function' ? cb : options.onClose;
                if (cb) cb(target);
            });

            return this;
        },

        listen: function listen(el) {

            if (typeof el === 'string') {
                var els = document.querySelectorAll(el),
                    i = els.length;
                while (i--) {
                    listen(els[i]);
                }
                return;
            }

            setStyle(el, {
                cursor: prefix + 'zoom-in'
            });

            el.addEventListener('click', function (e) {
                e.stopPropagation();
                if (shown) {
                    api.close();
                } else {
                    api.open(el);
                }
            });

            return this;
        }
    };

    overlay.addEventListener('click', api.close);
    wrapper.addEventListener('click', api.close);

    // umd expose
    if (( false ? 'undefined' : _typeof(exports)) == "object") {
        module.exports = api;
    } else if (true) {
        !(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
            return api;
        }).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
    } else {
        this.Zoomerang = api;
    }
})();

/***/ }),

/***/ 62:
/***/ (function(module, exports) {

/*!
Mailchimp Ajax Submit
jQuery Plugin
Author: Siddharth Doshi

Use:
===
$('#form_id').ajaxchimp(options);

- Form should have one <input> element with attribute 'type=email'
- Form should have one label element with attribute 'for=email_input_id' (used to display error/success message)
- All options are optional.

Options:
=======
options = {
    language: 'en',
    callback: callbackFunction,
    url: 'http://blahblah.us1.list-manage.com/subscribe/post?u=5afsdhfuhdsiufdba6f8802&id=4djhfdsh99f'
}

Notes:
=====
To get the mailchimp JSONP url (undocumented), change 'post?' to 'post-json?' and add '&c=?' to the end.
For e.g. 'http://blahblah.us1.list-manage.com/subscribe/post-json?u=5afsdhfuhdsiufdba6f8802&id=4djhfdsh99f&c=?',
*/

(function ($) {
    'use strict';

    $.ajaxChimp = {
        responses: {
            'We have sent you a confirmation email': 0,
            'Please enter a value': 1,
            'An email address must contain a single @': 2,
            'The domain portion of the email address is invalid (the portion after the @: )': 3,
            'The username portion of the email address is invalid (the portion before the @: )': 4,
            'This email address looks fake or invalid. Please enter a real email address': 5
        },
        translations: {
            'en': null
        },
        init: function init(selector, options) {
            $(selector).ajaxChimp(options);
        }
    };

    $.fn.ajaxChimp = function (options) {
        $(this).each(function (i, elem) {
            var form = $(elem);
            var email = form.find('input[type=email]');
            var label = form.find('label[for=' + email.attr('id') + ']');

            var settings = $.extend({
                'url': form.attr('action'),
                'language': 'en'
            }, options);

            var url = settings.url.replace('/post?', '/post-json?').concat('&c=?');

            form.attr('novalidate', 'true');
            email.attr('name', 'EMAIL');

            form.submit(function () {
                var msg;
                function successCallback(resp) {
                    if (resp.result === 'success') {
                        msg = 'We have sent you a confirmation email';
                        label.removeClass('error').addClass('valid');
                        email.removeClass('error').addClass('valid');
                    } else {
                        email.removeClass('valid').addClass('error');
                        label.removeClass('valid').addClass('error');
                        var index = -1;
                        try {
                            var parts = resp.msg.split(' - ', 2);
                            if (parts[1] === undefined) {
                                msg = resp.msg;
                            } else {
                                var i = parseInt(parts[0], 10);
                                if (i.toString() === parts[0]) {
                                    index = parts[0];
                                    msg = parts[1];
                                } else {
                                    index = -1;
                                    msg = resp.msg;
                                }
                            }
                        } catch (e) {
                            index = -1;
                            msg = resp.msg;
                        }
                    }

                    // Translate and display message
                    if (settings.language !== 'en' && $.ajaxChimp.responses[msg] !== undefined && $.ajaxChimp.translations && $.ajaxChimp.translations[settings.language] && $.ajaxChimp.translations[settings.language][$.ajaxChimp.responses[msg]]) {
                        msg = $.ajaxChimp.translations[settings.language][$.ajaxChimp.responses[msg]];
                    }
                    label.html(msg);

                    label.show(2000);
                    if (settings.callback) {
                        settings.callback(resp);
                    }
                }

                var data = {};
                var dataArray = form.serializeArray();
                $.each(dataArray, function (index, item) {
                    data[item.name] = item.value;
                });

                $.ajax({
                    url: url,
                    data: data,
                    success: successCallback,
                    dataType: 'jsonp',
                    error: function error(resp, text) {
                        console.log('mailchimp ajax submit error: ' + text);
                    }
                });

                // Translate and display submit message
                var submitMsg = 'Submitting...';
                if (settings.language !== 'en' && $.ajaxChimp.translations && $.ajaxChimp.translations[settings.language] && $.ajaxChimp.translations[settings.language]['submit']) {
                    submitMsg = $.ajaxChimp.translations[settings.language]['submit'];
                }
                label.html(submitMsg).show(2000);

                return false;
            });
        });
        return this;
    };
})(jQuery);

/***/ })

/******/ });