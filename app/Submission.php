<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    public function user_id(){
        return $this->hasOne(User::class);
    }
    public function category_id(){
        return $this->hasOne(Category::class);
    }
}
