<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function Submission(){
        return $this->hasMany(Submission::class);
    }
}
