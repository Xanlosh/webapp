<?php

namespace App\Http\Resources;

use App\Category;
use App\User;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class SubmissionResource
 * @package App\Http\Resources
 */
class SubmissionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     * Todo Check for null values
     */
    public function toArray($request)
    {
        $array = [];

        if ($this[0] !== null) {
            // multiple items are requested
            foreach ($this->resource as $item) {
                $subArray = [
                    'id' => $item->id,
                    'file_uri' => $item->fileUri,
                    'created_at' => $item->created_at,
                    'updated_at' => $item->updated_at,
                    'category' => Category::find($item->category_id),
                    'user' => User::find($item->user_id),
                    'comment' => $item->comment,
                    'validated' => $item->validated
                ];
                array_push($array, $subArray);
            }

        } else {
            // only one item request
            $array = [
                'id' => $this->id,
                'file_uri' => $this->fileUri,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
                'category' => Category::find($this->category_id),
                'user' => User::find($this->user_id),
                'comment' => $this->comment,
                'validated' => $this->validated

            ];
        }
        return $array;
    }
}
