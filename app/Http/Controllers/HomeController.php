<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewSubmission($request){
        return view('submission');
    }
    public function index()
    {
        if (Auth::user()->role->name === 'admin') {
            return view('home-admin');
        } else {
            return view('home');
        }
    }
}
