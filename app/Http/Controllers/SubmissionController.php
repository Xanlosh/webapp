<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\SubmissionResource;
use App\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class SubmissionController extends Controller
{
    /* Todo cleanup roles check */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * @param $id
     * @return SubmissionResource|array
     */
    public function show($id)
    {
        if (Auth::user()->role->name === 'admin') {
            return new SubmissionResource(Submission::find($id));
        } elseif (Submission::find($id)->user_id == Auth::user()->id) {
            return new SubmissionResource(Submission::find($id));
        } else {
            return ["ERROR" => "There are no results with the current permissions"];
        }

    }

    public function getCategories(){
        return Category::all();
    }
    public function getUserSubmissionsByCategory($request){
        return new SubmissionResource(Submission::where('user_id', Auth::user()->id)->where('category_id',$request)->get());
    }
    public function all()
    {
        if (Auth::user()->role->name === 'admin') {
            return new SubmissionResource(Submission::all());
        } else {
            return new SubmissionResource(Submission::where('user_id', Auth::user()->id)->get());
        }
    }
}
