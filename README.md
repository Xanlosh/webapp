# Webapp
## Documentation on the Directory structure

https://laravel.com/docs/5.6/structure

## How to deploy
Install Composer https://getcomposer.org/

Install NPM https://nodejs.org/en/

Copy the cloned repository to the server web root and change the root to {oldroot}\public

run the composer update command -> if this fails install the mentioned php extensions in the console output

run npm install and compile js and scss using npm run dev

rename the .env-example into .env

configure the database connection in the .env file

run the php artisan migrate command in the cloned repository

generate a encryption key using php artisan key:generate

generate oauth keys to use the internal api using php artisan passport:install 

The admin panel can be reached at localhost/admin after using the command 

php artisan voyager:install --with-dummy 



